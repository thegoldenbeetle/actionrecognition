#!/usr/bin/env python3
import lightning as L
import torch
import torch.nn.functional as F
import torchmetrics
from torch import nn, optim


class ResNeSt(L.LightningModule):
    def __init__(
        self,
            pretrain: bool = True,
    ):
        super().__init__()
        self.save_hyperparameters()

        self.model = torch.hub.load(
            "zhanghang1989/ResNeSt", "resnest50", pretrained=pretrain,
        )
        for param in self.model.parameters():
            param.requires_grad = False
        for param in self.model.avgpool.parameters():
            param.requires_grad = True
        for param in self.model.layer4.parameters():
            param.requires_grad = True
        self.model.fc = nn.Linear(in_features=2048, out_features=15, bias=True)

        self.val_acc = torchmetrics.Accuracy(task="multiclass", num_classes=15)
        self.val_f1 = torchmetrics.F1Score(task="multiclass", num_classes=15)
        self.test_acc = torchmetrics.Accuracy(task="multiclass", num_classes=15)
        self.test_f1 = torchmetrics.F1Score(task="multiclass", num_classes=15)

    def forward(self, x):
        orig_shape = x.shape
        x = x.transpose(1, 2).reshape((-1, x.shape[1], *x.shape[3:]))
        x = self.model(x)
        x = x.reshape((orig_shape[0], orig_shape[2], -1))
        return x

    def _calculate_loss(self, pred, labels):
        expand_labels = (
            labels.reshape(1, -1)
            .expand(pred.shape[1], labels.shape[0])
            .transpose(0, 1)
            .flatten()
        )
        return F.cross_entropy(pred.reshape((-1, pred.shape[2])), expand_labels)

    def training_step(self, batch, batch_idx):
        del batch_idx
        batch_size = batch["video"].shape[0]
        pred = self(batch["video"])
        loss = self._calculate_loss(pred, batch["label"])
        self.log("train/loss", loss, prog_bar=True, batch_size=batch_size)
        return loss

    def validation_step(self, batch, batch_idx):
        del batch_idx
        batch_size = batch["video"].shape[0]
        pred = self(batch["video"])
        loss = self._calculate_loss(pred, batch["label"])
        self.log("val/loss", loss, prog_bar=True, batch_size=batch_size)

        self.val_acc(pred.argmax(dim=2).median(dim=1).values, batch["label"])
        self.log("val/acc", self.val_acc)

        self.val_f1(pred.argmax(dim=2).median(dim=1).values, batch["label"])
        self.log("val/F1", self.val_f1)
        return loss

    def test_step(self, batch, batch_idx):
        del batch_idx
        batch_size = batch["video"].shape[0]
        pred = self(batch["video"])
        loss = self._calculate_loss(pred, batch["label"])
        self.log("test/loss", loss, prog_bar=True, batch_size=batch_size)

        self.test_acc(pred.argmax(dim=2).median(dim=1).values, batch["label"])
        self.log("test/acc", self.test_acc)

        self.test_f1(pred.argmax(dim=2).median(dim=1).values, batch["label"])
        self.log("test/F1", self.test_f1)
        return loss

    def configure_optimizers(self):
        return optim.Adam(self.parameters(), lr=1e-1)
