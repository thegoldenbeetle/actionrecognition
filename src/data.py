import os
from pathlib import Path
from typing import Optional, Union

from lightning import LightningDataModule
from pytorchvideo.data import Kinetics, make_clip_sampler
from pytorchvideo.transforms import (
    ApplyTransformToKey,
    ShortSideScale,
    UniformTemporalSubsample,
)
from torch.utils.data import ConcatDataset, DataLoader
from torchvision.transforms import Compose, Lambda
from torchvision.transforms._transforms_video import CenterCropVideo, NormalizeVideo


class KineticVideoDataModule(LightningDataModule):
    def __init__(
        self,
        data_dir: Union[str, Path],
        train_transform=None,
        test_transform=None,
        batch_size: int = 64,
        num_workers: Optional[int] = None,
        clip_duration: int = 2,
        side_size: int = 256,
        crop_size: int = 256,
        num_frames: int = 8,
        sampling_rate: int = 8,
        frames_per_second: int = 30,
    ):
        super().__init__()

        self.data_dir = Path(data_dir)
        self.side_size = side_size
        self.crop_size = crop_size
        self.num_frames = num_frames
        self.sampling_rate = sampling_rate
        self.frames_per_second = frames_per_second
        self.clip_duration = clip_duration
        self.batch_size = batch_size
        self.num_workers = num_workers
        if num_workers is None:
            self.num_workers = os.cpu_count()

        self.train_transform = train_transform
        if train_transform is None:
            self.train_transform = self.default_train_transform()

        self.test_transform = test_transform
        if test_transform is None:
            self.test_transform = self.default_test_transform()

    def default_train_transform(self):
        return self.default_test_transform()

    def default_test_transform(self):
        mean = [0.45, 0.45, 0.45]
        std = [0.225, 0.225, 0.225]
        return ApplyTransformToKey(
            key="video",
            transform=Compose(
                [
                    UniformTemporalSubsample(self.num_frames),
                    Lambda(lambda x: x / 255.0),
                    NormalizeVideo(mean, std),
                    ShortSideScale(size=self.side_size),
                    CenterCropVideo(crop_size=(self.crop_size, self.crop_size)),
                ]
            ),
        )

    def setup(self, stage=None):
        del stage
        self.train_data = Kinetics(
            data_path=self.data_dir / "train",
            clip_sampler=make_clip_sampler("random", self.clip_duration),
            decode_audio=False,
            transform=self.train_transform,
        )
        self.val_data = Kinetics(
            data_path=self.data_dir / "validate",
            clip_sampler=make_clip_sampler("uniform", self.clip_duration),
            decode_audio=False,
            transform=self.test_transform,
        )

    def train_dataloader(self):
        return DataLoader(
            self.train_data,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_data,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
        )
