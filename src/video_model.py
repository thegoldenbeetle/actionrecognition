#!/usr/bin/env python3
import lightning as L
import torch
import torch.nn.functional as F
import torchmetrics
from torch import nn, optim


class ResNet3D(L.LightningModule):
    def __init__(
        self,
            pretrain: bool = True
    ):
        super().__init__()
        self.save_hyperparameters()

        self.model = self._load_model(pretrain=pretrain)
        self.val_acc = torchmetrics.Accuracy(task="multiclass", num_classes=15)
        self.val_f1 = torchmetrics.F1Score(task="multiclass", num_classes=15)
        self.test_acc = torchmetrics.Accuracy(task="multiclass", num_classes=15)
        self.test_f1 = torchmetrics.F1Score(task="multiclass", num_classes=15)

    def _load_model(self, pretrain: bool = True):
        model = torch.hub.load(
            "facebookresearch/pytorchvideo",
            "slow_r50",
            pretrained=pretrain,
        )
        for param in model.parameters():
            param.requires_grad = False
        for param in model.blocks[4].parameters():
            param.requires_grad = True
        for param in model.blocks[5].parameters():
            param.requires_grad = True
        model.blocks[5].proj = nn.Linear(
            in_features=2048,
            out_features=15,
            bias=True,
        )
        return model

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        del batch_idx
        batch_size = batch["video"].shape[0]
        pred = self(batch["video"])
        loss = F.cross_entropy(pred, batch["label"])
        self.log("train/loss", loss, prog_bar=True, batch_size=batch_size)
        return loss

    def validation_step(self, batch, batch_idx):
        del batch_idx
        batch_size = batch["video"].shape[0]
        pred = self(batch["video"])
        loss = F.cross_entropy(pred, batch["label"])
        self.log("val/loss", loss, prog_bar=True, batch_size=batch_size)

        self.val_acc(pred.argmax(dim=1), batch["label"])
        self.log("val/acc", self.val_acc)

        self.val_f1(pred.argmax(dim=1), batch["label"])
        self.log("val/F1", self.val_f1)
        return loss

    def test_step(self, batch, batch_idx):
        del batch_idx
        batch_size = batch["video"].shape[0]
        pred = self(batch["video"])
        loss = F.cross_entropy(pred, batch["label"])
        self.log("test/loss", loss, prog_bar=True, batch_size=batch_size)

        self.test_acc(pred.argmax(dim=1), batch["label"])
        self.log("test/acc", self.test_acc)

        self.test_f1(pred.argmax(dim=1), batch["label"])
        self.log("test/F1", self.test_f1)
        return loss


    def configure_optimizers(self):
        return optim.Adam(self.parameters(), lr=1e-1)
