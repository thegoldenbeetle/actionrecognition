# Action recognition

## Описание

## Обучение 

- Установить окружение:
``` shell
> export DOCKER_UID=1000
> export DOCKER_GID=1000
> docker-compose build
```

- Запуск окружения:
``` shell
> docker-compose run workspace
```

- Скачать датасет используя скрипт `scripts/download.py`:

``` shell
> python3 -m scripts.download
```

`

- Запустить скрипт:
``` shell
> python3 -m scripts.train_video # Для ResNet3D
> python3 -m scripts.train_img   # Для ResNeSt
```

## Результаты

## По видео (ResNet3D)

Train loss:

![Train loss](imgs/video_train_loss.png "train_loss")

Validation loss:

![Val loss](imgs/video_val_loss.png "val_loss")

Validation accuracy:

![Accuracy](imgs/video_acc.png "accuracy")

Validation F1:

![F1](imgs/video_f1.png "F1")

## По изображениям (ResNeSt)

Результатом был медианный предсказанный класс по всем кадрам.

Train loss:

![Train loss](imgs/img_train_loss.png "train_loss")

Validation loss:

![Val loss](imgs/img_val_loss.png "val_loss")

Validation accuracy:

![Accuracy](imgs/img_acc.png "accuracy")

Validation F1:

![F1](imgs/img_f1.png "F1")

## Метрики

| Test metric | ResNet3D | ResNeSt |
|:-----------:|:--------:|:-------:|
| test/F1     | 0.55129  | 0.43632 |
| test/acc    | 0.55129  | 0.43632 |
| test/loss   | 1.42098  | 1.87734 |

