import torch
from lightning.pytorch import Trainer, seed_everything
from lightning.pytorch.callbacks import ModelCheckpoint
from lightning.pytorch.loggers import TensorBoardLogger

from src.data import KineticVideoDataModule
from src.video_model import ResNet3D

if __name__ == "__main__":
    seed_everything(9324, workers=True)
    torch.set_float32_matmul_precision("high")
    logger = TensorBoardLogger(save_dir="logs", version=1, name="VideoModel")
    model = ResNet3D()
    # model = ResNet3D.load_from_checkpoint("logs/VideoModel/version_1/checkpoints/epoch=9-step=3960.ckpt", pretrain=False)
    datamodule = KineticVideoDataModule(
        "./data/kinetics700_2020",
        batch_size=32,
        num_workers=4,
    )
    checkpoint_callback = ModelCheckpoint(monitor="val/loss")
    trainer = Trainer(
        default_root_dir="logs",
        max_epochs=150,
        callbacks=[checkpoint_callback],
        logger=logger,
    )
    trainer.fit(model, datamodule)
    # datamodule.setup()
    # trainer.test(model, datamodule.val_dataloader())
