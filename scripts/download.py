import argparse
import glob
import json
import os
import shutil
import subprocess
import time
import uuid
from collections import OrderedDict

import pandas as pd
import yt_dlp
from joblib import Parallel, delayed
from tqdm.auto import tqdm


def create_video_folders(dataset, output_dir):
    """Creates a directory for each label name in the dataset."""
    if "label-name" not in dataset.columns:
        this_dir = os.path.join(output_dir, "test")
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        # I should return a dict but ...
        return this_dir
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    label_to_dir = {}
    for label_name in dataset["label-name"].unique():
        this_dir = os.path.join(output_dir, label_name)
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        label_to_dir[label_name] = this_dir
    return label_to_dir


def construct_video_filename(row, label_to_dir, trim_format="%06d"):
    """Given a dataset row, this function constructs the
    output filename for a given video.
    """
    basename = "%s_%s_%s.mp4" % (
        row["video-id"],
        trim_format % row["start-time"],
        trim_format % row["end-time"],
    )
    if not isinstance(label_to_dir, dict):
        dirname = label_to_dir
    else:
        dirname = label_to_dir[row["label-name"]]
    output_filename = os.path.join(dirname, basename)
    return output_filename


def download_clip(
    video_identifier,
    output_filename,
    start_time,
    end_time,
    num_attempts=10,
    url_base="https://www.youtube.com/watch?v=",
):
    """Download a video from youtube if exists and is not blocked.

    arguments:
    ---------
    video_identifier: str
        Unique YouTube video identifier (11 characters)
    output_filename: str
        File path where the video will be stored.
    start_time: float
        Indicates the begining time in seconds from where the video
        will be trimmed.
    end_time: float
        Indicates the ending time in seconds of the trimmed video.
    """
    # Defensive argument checking.
    assert isinstance(video_identifier, str), "video_identifier must be string"
    assert isinstance(output_filename, str), "output_filename must be string"
    assert len(video_identifier) == 11, "video_identifier must have length 11"

    status = False

    class EmptyLogger:
        def error(msg):
            ...

        def warning(msg):
            ...

        def debug(msg):
            ...

    ydl_opts = {
        "format": "mp4",
        "quiet": True,
        "no_warnings": True,
        "outtmpl": output_filename,
        "download_ranges": lambda x, y: [
            {"start_time": int(start_time), "end_time": int(end_time)}
        ],
        "logger": EmptyLogger,
    }
    attempts = 0
    while True:
        try:
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                ydl.download(url_base + video_identifier)
        except Exception as err:
            attempts += 1
            if attempts == num_attempts:
                return status, str(err)
        else:
            break
    # Check if the video was successfully saved.
    status = os.path.exists(output_filename)
    return status, "Downloaded"


def download_clip_wrapper(row, label_to_dir, trim_format):
    """Wrapper for parallel processing purposes."""
    output_filename = construct_video_filename(row, label_to_dir, trim_format)
    clip_id = os.path.basename(output_filename).split(".mp4")[0]
    if os.path.exists(output_filename):
        status = tuple([clip_id, True, "Exists"])
        return status

    downloaded, log = download_clip(
        row["video-id"],
        output_filename,
        row["start-time"],
        row["end-time"],
    )
    status = tuple([clip_id, downloaded, log])
    return status


def parse_kinetics_annotations(input_csv, ignore_is_cc=False):
    """Returns a parsed DataFrame.

    arguments:
    ---------
    input_csv: str
        Path to CSV file containing the following columns:
          'YouTube Identifier,Start time,End time,Class label'

    returns:
    -------
    dataset: DataFrame
        Pandas with the following columns:
            'video-id', 'start-time', 'end-time', 'label-name'
    """
    df = pd.read_csv(input_csv)
    if "youtube_id" in df.columns:
        columns = OrderedDict(
            [
                ("youtube_id", "video-id"),
                ("time_start", "start-time"),
                ("time_end", "end-time"),
                ("label", "label-name"),
            ]
        )
        df.rename(columns=columns, inplace=True)
        if ignore_is_cc:
            df = df.loc[:, df.columns.tolist()[:-1]]
    return df


def main(
    input_csv,
    output_dir,
    trim_format="%06d",
    num_jobs=24,
    labels_regex="dancing",
    drop_duplicates=False,
):
    # Reading and parsing Kinetics.
    dataset = parse_kinetics_annotations(input_csv)
    if labels_regex:
        dataset = dataset[dataset["label-name"].str.contains(labels_regex)]

    # Creates folders where videos will be saved later.
    label_to_dir = create_video_folders(dataset, output_dir)

    # Download all clips.
    if num_jobs == 1:
        status_lst = []
        for i, row in tqdm(dataset.iterrows(), total=len(dataset)):
            status_lst.append(download_clip_wrapper(row, label_to_dir, trim_format))
    else:
        status_lst = Parallel(n_jobs=num_jobs)(
            delayed(download_clip_wrapper)(row, label_to_dir, trim_format)
            for i, row in tqdm(dataset.iterrows(), total=len(dataset))
        )

    # Save download report.
    with open("download_report.json", "w") as fobj:
        fobj.write(json.dumps(status_lst))


if __name__ == "__main__":
    description = "Helper script for downloading and trimming kinetics videos."
    p = argparse.ArgumentParser(description=description)
    p.add_argument(
        "input_csv",
        type=str,
        help=(
            "CSV file containing the following format: "
            "YouTube Identifier,Start time,End time,Class label"
        ),
    )
    p.add_argument(
        "output_dir", type=str, help="Output directory where videos will be saved."
    )
    p.add_argument(
        "-r",
        "--labels-regex",
        type=str,
        default="dancing",
    )
    p.add_argument(
        "-f",
        "--trim-format",
        type=str,
        default="%06d",
        help=(
            "This will be the format for the "
            "filename of trimmed videos: "
            "videoid_%0xd(start_time)_%0xd(end_time).mp4"
        ),
    )
    p.add_argument("-n", "--num-jobs", type=int, default=8)
    p.add_argument(
        "--drop-duplicates",
        type=str,
        default="non-existent",
        help="Unavailable at the moment",
    )
    # help='CSV file of the previous version of Kinetics.')
    main(**vars(p.parse_args()))
